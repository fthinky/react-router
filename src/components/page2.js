import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Route, Switch } from "react-router-dom";
import Alt1 from './alt1';
import Alt2 from './alt2';
class Page2 extends Component {
  constructor(props) {
    super(props);
  }
  navigateToPage1 = () => {
    this.props.history.push('/sayfa1/');
  }
  render() {
    return (
      <div>
        Page2
        <button onClick={this.navigateToPage1}>Sayfa1</button>
        <Switch>
        <Route exact path="/sayfa2" component={Alt1} />
            <Route path="/sayfa2/alt1" component={Alt1} />
            <Route path="/sayfa2/alt2" component={Alt2} />
          </Switch>
      </div>
    )
  }
}
// export default Page2;
 export default withRouter(Page2);