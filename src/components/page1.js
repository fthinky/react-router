import React, { Component } from 'react'
import { Redirect } from "react-router-dom";
import {withRouter} from 'react-router-dom'

class Page1 extends Component {
constructor(props){
  super(props);
}

navigateToPage2=()=>{
  this.props.history.push('/sayfa2/');
  // window.location.href="/#/sayfa2/"
}


  render() {
    return (
      <div>
          Page1
          <button onClick={this.navigateToPage2}>Sayfa2</button>
      </div>
    )
  }
}
export default Page1;
// export default withRouter(Page1);