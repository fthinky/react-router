import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import Page1 from './components/page1'
import Page2 from './components/page2'
import { NoMatch } from './components/noMatch'
import { HashRouter as Router, Route, Link, Switch } from "react-router-dom";
// import '../style.css';
export default class App extends Component {
  render() {
    return (<div><Router>
      <div className="form-group row">
        <div className="col-md-3">
          <div>
            <nav>
              <ul>
                <li>
                  <Link to="/sayfa1/">Sayfa1</Link>
                </li>
                <li>
                  <Link to="/sayfa2/">Sayfa2</Link>
                </li>
              </ul>
            </nav>
          </div>

        </div>
        <div className="col-md-9">
          <Switch>
            <Route exact path="/" component={Page1} />
            <Route path="/sayfa1/" component={Page1} />
            <Route path="/sayfa2/" component={Page2} />
            {/* <Route path="/about" render={props => <About {...props} extra={someVariable} />}/> */}

            <Route component={NoMatch} />
          </Switch>
        </div>

      </div>
    </Router>  </div>
    );
  }
}



ReactDOM.render(<App />, document.getElementById("container"));
